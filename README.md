# CloudOpss Challenge

This project's purpose is to fullfill the [challenge](CHALLENGE.md) made by CloudOpss.

## Getting started

You'll need docker, docker-compose and git to run this code.

1 - Start by clonning this repository

    # If you have a ssh git config:
    git clone git@gitlab.com:osouza.de/desafio-cloudopss.git

    # Otherwise, you can clone the repo with HTTPS as well
    git clone https://gitlab.com/osouza.de/desafio-cloudopss.git

2 - Create a ".env" file with the following variables

    MONGO_HOST:"database"
    MONGO_PORT:"27017"
    MONGO_INITDB_ROOT_USERNAME:"root"
    MONGO_INITDB_ROOT_PASSWORD:"t00h4rd2crack"
    MONGO_INITDB_DATABASE:"CloudOpss"
    MONGO_INITDB_USER:"user"
    MONGO_INITDB_PWD:"t00h4rd2cracks"
    MONGO_DETAILS:"mongodb://${MONGO_INITDB_USER}:${MONGO_INITDB_PWD}@${MONGO_HOST}:${MONGO_PORT}/${MONGO_INITDB_DATABASE}?authSource=${MONGO_INITDB_DATABASE}&retryWrites=true&w=majority"

**Note** some systems don't accept .env files to docker compose (as docker-compose version 1.24.0).

If it is your case, edit the docker-compose variables instead of counting on .env file.

3 - Get everything working:

    docker-compose up

This'll:
  - download the required images
  - build the images and the applications inside of it (frontend and backend)
  - setup the database
  - and serve the project exposing the ports 5000 (backend), 4200 (frontend), 8080(webserver), 27017(mongodb)

So you can access the system there:

Frontend interface via "<nginx:8080/>" and the backend in "<nginx:8080/api/(docs/redocs)?>

### Accessing gitpod env Chrome browser :
TODO


## Documentation
The API is documented with Swagger and ReDoc. Feel free to pick the UI that better suits you. ;)
URLs:

 - "<nginx:8080>/api/docs" to the Swagger version
 - "<nginx:8080>/api/redocs" to the ReDocs.

## Logging

You can check the application logs by:

- Running docker compose logs to get the whole solutions logs: `docker-compose logs`
- Attaching to some container: `docker attach backend` (note it'll kill the container if you exit)
- Starting the docker-compose without detached (-d) option: `docker-compose up`

## Prebuilt docker images

@TODO

It's still work to be done.

## Production

@TODO

It would be nice if we did production build versions... but the time is over.
