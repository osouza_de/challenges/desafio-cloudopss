/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import type { Observable } from 'rxjs';

import type { CustomerSchema } from '../models/CustomerSchema';
import type { UpdateCustomerModel } from '../models/UpdateCustomerModel';

import { OpenAPI } from '../core/OpenAPI';
import { request as __request } from '../core/request';

@Injectable()
export class CustomerService {

    constructor(public readonly http: HttpClient) {}

    /**
     * Route List Customers
     * @returns any Retrieve a list of every customer
     * @throws ApiError
     */
    public routeListCustomersCustomerGet(): Observable<any> {
        return __request(OpenAPI, this.http, {
            method: 'GET',
            url: '/customer/',
        });
    }

    /**
     * Route Create Customer
     * @param requestBody
     * @returns any Insert a new customer data into the system
     * @throws ApiError
     */
    public routeCreateCustomerCustomerPost(
        requestBody: CustomerSchema,
    ): Observable<any> {
        return __request(OpenAPI, this.http, {
            method: 'POST',
            url: '/customer/',
            body: requestBody,
            mediaType: 'application/json',
            errors: {
                422: `Validation Error`,
            },
        });
    }

    /**
     * Route Read Customer
     * @param id
     * @returns any Retrieve a customer's data
     * @throws ApiError
     */
    public routeReadCustomerCustomerIdGet(
        id: any,
    ): Observable<any> {
        return __request(OpenAPI, this.http, {
            method: 'GET',
            url: '/customer/{id}',
            path: {
                'id': id,
            },
            errors: {
                422: `Validation Error`,
            },
        });
    }

    /**
     * Route Update Customer
     * @param id
     * @param requestBody
     * @returns any Successful Response
     * @throws ApiError
     */
    public routeUpdateCustomerCustomerIdPut(
        id: string,
        requestBody: UpdateCustomerModel,
    ): Observable<any> {
        return __request(OpenAPI, this.http, {
            method: 'PUT',
            url: '/customer/{id}',
            path: {
                'id': id,
            },
            body: requestBody,
            mediaType: 'application/json',
            errors: {
                422: `Validation Error`,
            },
        });
    }

    /**
     * Route Delete Customer
     * @param id
     * @returns any Delete a customer data from the system
     * @throws ApiError
     */
    public routeDeleteCustomerCustomerIdDelete(
        id: string,
    ): Observable<any> {
        return __request(OpenAPI, this.http, {
            method: 'DELETE',
            url: '/customer/{id}',
            path: {
                'id': id,
            },
            errors: {
                422: `Validation Error`,
            },
        });
    }

}