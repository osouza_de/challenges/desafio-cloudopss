/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type CustomerSchema = {
    fullname?: string;
    email: string;
    phone: string;
    address_street: string;
    address_number: string;
    address_district: string;
    address_city: string;
    address_state: string;
    address_zipcode: string;
    address_complement: string;
    occupation: string;
    resume: string;
};
