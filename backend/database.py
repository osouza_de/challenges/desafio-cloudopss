import motor.motor_asyncio
from bson.objectid import ObjectId
from decouple import config

MONGO_DETAILS = config("MONGO_DETAILS")

client = motor.motor_asyncio.AsyncIOMotorClient(MONGO_DETAILS)
database = client.get_default_database()
customer_collection = database.get_collection("Customers")

def customer_helper(customer) -> dict:
    return {
        "id": str(customer["_id"]),
        "fullname": customer["fullname"],
        "email": customer["email"],
        "phone": customer["phone"],
        "address_street": customer["address_street"],
        "address_number": customer["address_number"],
        "address_district": customer["address_district"],
        "address_city": customer["address_city"],
        "address_state": customer["address_state"],
        "address_zipcode": customer["address_zipcode"],
        "address_complement": customer["address_complement"],
        "occupation": customer["occupation"],
        "resume": customer["resume"],
    }

# Get every customer present in the database
async def list_customers():
    customers = []
    async for customer in customer_collection.find():
        customers.append(customer_helper(customer))
    return customers

# Add a new customer into to the database
async def create_customer(customer_data: dict) -> dict:
    customer = await customer_collection.insert_one(customer_data)
    new_customer = await customer_collection.find_one({"_id": customer.inserted_id})
    return customer_helper(new_customer)

# Retrieve a customer with a matching ID
async def read_customer(id: str) -> dict:
    customer = await customer_collection.find_one({"_id": ObjectId(id)})
    if customer:
        return customer_helper(customer)

# Update a customer with a matching ID
async def update_customer(id: str, data: dict):
    if len(data) < 1:
        return False
    customer = await customer_collection.find_one({"_id": ObjectId(id)})
    if customer:
        updated_customer = await customer_collection.update_one(
            {"_id": ObjectId(id)}, {"$set": data}
        )
        if updated_customer:
            return True
        return False

# Delete a customer from the database
async def delete_customer(id: str):
    customer = await customer_collection.find_one({"_id": ObjectId(id)})
    if customer:
        await customer_collection.delete_one({"_id": ObjectId(id)})
        return True
